;;;; utils.lisp
(in-package #:gk-ui-rpg-example)

(defun hexcolor (hexcode &optional (a 1))
  (let* ((color-string (subseq hexcode 1))
         (r (float (/ (parse-integer (subseq color-string 0 2) :radix 16) 255)))
         (g (float (/ (parse-integer (subseq color-string 2 4) :radix 16) 255)))
         (b (float (/ (parse-integer (subseq color-string 4 6) :radix 16) 255))))
    (vec4 r g b a)))

(defun alpha-color (color alpha)
  (vec4 (x color) (y color) (z color) alpha))

(defmacro with-bind-buttons (() &body body)
  (let ((result (list 'progn)))
    (dolist (b body (reverse result))
      (destructuring-bind (key state) (first b)
        (push `(bind-button ,key ,state (lambda () ,@(rest b)))
              result)))))

(defun form-to-string (form) (format nil "~A" form))
