;;;; resources.lisp
(in-package #:gk-ui-rpg-example)

(defun asset-path (pathname)
  (asdf:system-relative-pathname :gk-ui/examples (merge-pathnames pathname "examples/rpg-example/assets/")))

(register-resource-package :gk-ui-rpg-example (asset-path ""))

;;; Images
(define-image :img-not-ok-sign (asset-path "not-ok-sign.png"))
(define-image :img-ok-sign (asset-path "ok-sign.png"))
(define-image :img-ui (asset-path "ui.png"))
(define-image :img-legs-lvl-2 (asset-path "body-parts/legs/lvl-2/legs-lvl-2.png"))
(define-image :img-legs-lvl-1 (asset-path "body-parts/legs/lvl-1/legs-lvl-1.png"))
(define-image :img-legs-lvl-0 (asset-path "body-parts/legs/lvl-0/legs-lvl-0.png"))
(define-image :img-arm-left-lvl-2 (asset-path "body-parts/arm-left/lvl-2/arm-left-lvl-2.png"))
(define-image :img-arm-left-lvl-1 (asset-path "body-parts/arm-left/lvl-1/arm-left-lvl-1.png"))
(define-image :img-arm-left-lvl-0 (asset-path "body-parts/arm-left/lvl-0/arm-left-lvl-0.png"))
(define-image :img-arm-right-lvl-2 (asset-path "body-parts/arm-right/lvl-2/arm-right-lvl-2.png"))
(define-image :img-arm-right-lvl-1 (asset-path "body-parts/arm-right/lvl-1/arm-right-lvl-1.png"))
(define-image :img-arm-right-lvl-0 (asset-path "body-parts/arm-right/lvl-0/arm-right-lvl-0.png"))
(define-image :img-body-lvl-2 (asset-path "body-parts/body/lvl-2/body-lvl-2.png"))
(define-image :img-body-lvl-1 (asset-path "body-parts/body/lvl-1/body-lvl-1.png"))
(define-image :img-body-lvl-0 (asset-path "body-parts/body/lvl-0/body-lvl-0.png"))
(define-image :img-head-lvl-2 (asset-path "body-parts/head/lvl-2/head-lvl-2.png"))
(define-image :img-head-lvl-1 (asset-path "body-parts/head/lvl-1/head-lvl-1.png"))
(define-image :img-head-lvl-0 (asset-path "body-parts/head/lvl-0/head-lvl-0.png"))
;;; Fonts
(define-font :fnt-fontasque-sans-mono-bold (asset-path "FantasqueSansMono-Bold.ttf"))
(define-font :fnt-fontasque-sans-mono-regular (asset-path "FantasqueSansMono-Regular.ttf"))


(defparameter *fnt-fontasque-sm-bold-14* nil)
(defparameter *fnt-fontasque-sm-bold-16* nil)
(defparameter *fnt-fontasque-sm-bold-18* nil)
(defparameter *fnt-fontasque-sm-bold-20* nil)
(defparameter *fnt-fontasque-sm-regular-14* nil)
(defparameter *fnt-fontasque-sm-regular-16* nil)
(defparameter *fnt-fontasque-sm-regular-18* nil)
(defparameter *fnt-fontasque-sm-regular-20* nil)
