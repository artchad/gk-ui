;;;; menu.lisp
(in-package #:gk-ui-rpg-example)

(defparameter *show-menu-p* nil)

(defun toggle-menu () (setf *show-menu-p* (not *show-menu-p*)))
