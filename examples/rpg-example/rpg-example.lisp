;;;; rpg-example.lisp
(in-package #:gk-ui-rpg-example)

;;; Global Variables
(defparameter *screen-width* 1280)
(defparameter *screen-height* 720)
(defparameter *panel-width* (/ (- *screen-width* (/ *screen-width* 5)) 2))
(defparameter *panel-height* (- *screen-height* (/ *screen-height* 5)))
(defparameter *panel-origin* (vec2 (/ (- *screen-width* *panel-width*) 16)
                                   (/ (- *screen-height* *panel-height*) 2)))

(setf *ui-font* *fnt-fontasque-sm-regular-18*)
(setf *ui-font-color* (hexcolor "#838E9B"))

;;; Update Methods
(defmethod update-label-alive ((label label)))
(defmethod update-label-dead ((label label)))
(defmethod update-label-sick ((label label)))
(defmethod update-label-killers ((label label)))
(defmethod update-status-bar ((bar status-bar)))

;;; Define Game
(defgame gk-ui-rpg-example ()
  ((panels :accessor panels :initform ()))
  (:viewport-width *screen-width*)
  (:viewport-height *screen-height*))

;;; Post Init
(defmethod post-initialize ((game gk-ui-rpg-example))
  (setf *fnt-fontasque-sm-bold-14* (make-font :fnt-fontasque-sans-mono-bold 14))
  (setf *fnt-fontasque-sm-bold-16* (make-font :fnt-fontasque-sans-mono-bold 16))
  (setf *fnt-fontasque-sm-bold-18* (make-font :fnt-fontasque-sans-mono-bold 18))
  (setf *fnt-fontasque-sm-bold-20* (make-font :fnt-fontasque-sans-mono-bold 20))
  (setf *fnt-fontasque-sm-regular-14* (make-font :fnt-fontasque-sans-mono-regular 14))
  (setf *fnt-fontasque-sm-regular-16* (make-font :fnt-fontasque-sans-mono-regular 16))
  (setf *fnt-fontasque-sm-regular-18* (make-font :fnt-fontasque-sans-mono-regular 18))
  (setf *fnt-fontasque-sm-regular-20* (make-font :fnt-fontasque-sans-mono-regular 20))


  ;; create player
  (setf *player* (make-player :hp 100 :hp-max 100
                              :mp 30 :mp-max 30
                              :atk 10 :def 10
                              :int 10 :ini 10
                              :xp 0 :next-lvl-xp 100))
  ;; reset panel
  (setf (panels (gamekit)) nil)

  (let* ((panel-origin (vec2 40 300))
         (panel-width 300)
         (panel-height 100)
         (panel-padding 0)
         ;; create panels
         (panel-0 (make-panel :align :left :origin panel-origin
                              :width panel-width :height 400 :bg-color (hexcolor "#282938") :padding 0))

         (panel-1 (make-panel :align :left :origin (add panel-origin (vec2 -30 102))
                              :width panel-width  :height panel-height :padding panel-padding))

         (panel-2 (make-panel :align :left :origin (add panel-origin (vec2 0 102))
                              :width panel-width  :height panel-height :padding panel-padding))
         (panel-3 (make-panel :align :left :origin (add panel-origin (vec2 144 188))
                              :width 150 :height 100 :padding 0))

         (seperator-panel (make-panel :align :left :origin (add panel-origin (vec2 0 113))
                                      :width panel-width :height panel-height))

         (bar-panel (make-panel :align :left :origin (add panel-origin (vec2 44 177))
                                :width 150 :height 100 :padding 0))
         ;; define bar parameters
         (bar-padding 7)
         (bar-row-span .6)
         (bar-width (- 150 40))
         ;; define element parameters
         (button-offset-1 (vec2 170 0))
         (button-height 15)
         (button-roundness 2)
         (button-offset (vec2 0 0))
         (*button-base-color* (hexcolor "#4C4570"))

         (offset-0 (vec2 10 0))
         (offset-1 (vec2 15 0))
         (offset-2 (vec2 15 -5))
         (button-row-span .68)
         ;; define status bars
         (xp-bar (make-instance 'status-bar :max-amount (next-lvl-xp *player*)
                                            :text "xp-bar"
                                            :filled-amount-0 0
                                            :filled-amount-0-color (hexcolor "#A4C960")
                                            :width bar-width
                                            :row-span bar-row-span
                                            :update (lambda (sb)
                                                      (setf (filled-amount-0 sb) (curr-lvl-xp *player*))
                                                      (setf (max-amount sb) (next-lvl-xp *player*)))
                                            :padding-vertical bar-padding))
         (hp-bar (make-instance 'status-bar :max-amount (hp-max *player*)
                                            :text "hp-bar"
                                            :row-span bar-row-span
                                            :filled-amount-0 (hp *player*)
                                            :filled-amount-0-color (hexcolor "#BB2C2C")
                                            :width bar-width
                                            :update (lambda (sb) (setf (filled-amount-0 sb) (hp *player*)))
                                            :padding-vertical bar-padding))
         (mp-bar (make-instance 'status-bar :max-amount (mp-max *player*)
                                            :text "mp-bar"
                                            :row-span bar-row-span
                                            :filled-amount-0 (mp *player*)
                                            :filled-amount-0-color (hexcolor "#4267BB")
                                            :width bar-width
                                            :update (lambda (sb) (setf (filled-amount-0 sb) (mp *player*)))
                                            :padding-vertical bar-padding))
         ;; these are all static labels (they don't track the value of a variable)
         (name (make-static-label :text "Name: Gilbert" :no-padding t :offset offset-0))
         (race (make-static-label :text "Race: Northmen" :no-padding t :offset offset-0))
         (class (make-static-label :text "Class: Warrior" :no-padding t :offset offset-0))
         (lvl (make-dynamic-label ("Level: ~A" (lvl *player*))
                                  :width 100
                                  :offset offset-2
                                  :row-span button-row-span))
         (xp (make-static-label :text "xp:" :no-padding t :offset offset-1))
         (hp (make-static-label :text "hp:" :no-padding t :offset offset-1))
         (mp (make-static-label :text "mp:" :no-padding t :offset offset-1))
         ;; these are all dynamic labels (they track the value of given expressions)
         (atk (make-dynamic-label ("atk: ~A" (atk *player*)) ; the expression in this case is (atk *player*)
                                  :width 100
                                  :offset offset-2
                                  :row-span button-row-span))
         (def (make-dynamic-label ("def: ~A" (def *player*))
                                  :width 100
                                  :offset offset-2
                                  :row-span button-row-span))
         (int (make-dynamic-label ("int: ~A" (int *player*))
                                  :width 100
                                  :offset offset-2
                                  :row-span button-row-span))
         (ini (make-dynamic-label ("ini: ~A" (ini *player*))
                                  :width 100
                                  :offset offset-2
                                  :row-span button-row-span))
         (dec-atk (make-button :offset button-offset-1 :text "-"
                               :height button-height :offset button-offset
                               :row-span button-row-span
                               :action player-atk-dec-5
                   :roundness button-roundness))
         (dec-def (make-button :offset button-offset-1 :text "-"
                               :height button-height :offset button-offset
                               :row-span button-row-span
                               :action player-def-dec-5
                               :roundness button-roundness))
         (dec-int (make-button :offset button-offset-1 :text "-"
                               :height button-height :offset button-offset
                               :row-span button-row-span
                               :action player-int-dec-5
                               :roundness button-roundness))
         (dec-ini (make-button :offset button-offset-1 :text "-"
                               :height button-height :offset button-offset
                               :row-span button-row-span
                               :action player-ini-dec-5
                               :roundness button-roundness))

         (inc-atk (make-button :offset button-offset-1 :text "+"
                              :height button-height :offset button-offset
                              :row-span button-row-span
                              :action player-atk-inc-5
                              :roundness button-roundness))
         (inc-def (make-button :offset button-offset-1 :text "+"
                              :height button-height :offset button-offset
                              :row-span button-row-span
                              :action player-def-inc-5
                              :roundness button-roundness))
         (inc-int (make-button :offset button-offset-1 :text "+"
                              :height button-height :offset button-offset
                              :row-span button-row-span
                              :action player-int-inc-5
                              :roundness button-roundness))
         (inc-ini (make-button :offset button-offset-1 :text "+"
                               :height button-height :offset button-offset
                               :row-span button-row-span
                               :action player-ini-inc-5
                               :roundness button-roundness))
         (sep-row-span .68)
         (sep-offset (vec2 0 3))
         (sep0 (make-separator :row-span sep-row-span :offset sep-offset))
         (sep1 (make-separator :row-span sep-row-span :offset sep-offset))
         (sep2 (make-separator :row-span sep-row-span :offset sep-offset))
         (sep3 (make-separator :row-span sep-row-span :offset sep-offset))
         (sep4 (make-separator :row-span sep-row-span :offset sep-offset))
         (dynlabel-fnt *fnt-fontasque-sm-regular-14*)
         (dynlabel-row-span .7)
         (xp-dynlabel (make-dynamic-label ("(~A/~A)" (curr-lvl-xp *player*)
                                                     (next-lvl-xp *player*))
                                          :font dynlabel-fnt
                                          :offset offset-2
                                          :row-span dynlabel-row-span))
         (hp-dynlabel (make-dynamic-label ("(~A/~A)" (hp *player*) (hp-max *player*))
                                          :font dynlabel-fnt
                                          :offset offset-2
                                          :row-span dynlabel-row-span))
         (mp-dynlabel (make-dynamic-label ("(~A/~A)" (mp *player*) (mp-max *player*))
                                          :font dynlabel-fnt
                                          :offset offset-2
                                          :row-span dynlabel-row-span)))
    ;; add elements to our panel
    (add-elements panel-0
      name race class
      lvl xp hp mp
      atk def int  ini)

    (add-elements panel-1
      dec-atk dec-def dec-int dec-ini)

    (add-elements panel-2
      inc-atk inc-def inc-int inc-ini)

    (add-elements panel-3
      xp-dynlabel hp-dynlabel mp-dynlabel)

    (add-elements seperator-panel
      sep0 sep1 sep2 sep3 sep4)

    (add-elements bar-panel
      xp-bar
      hp-bar
      mp-bar)

    (push panel-0 (panels (gamekit)))
    (push panel-1 (panels (gamekit)))
    (push panel-2 (panels (gamekit)))
    (push panel-3 (panels (gamekit)))
    (push seperator-panel (panels (gamekit)))
    (push bar-panel (panels (gamekit)))
    ;; reverse panel order to ensure correct draw order
    (setf (panels (gamekit)) (nreverse (panels (gamekit)))))


  (with-bind-buttons ()
    ((:escape :pressed) (stop))

    ((:q :pressed) (stop))

    ((:s :pressed) (toggle-menu))

    ((:l :pressed) (add-xp 32))

    ((:d :pressed) (damage-for 8 *player*))

    ((:h :pressed) (heal-for 8 *player*))

    ((:mouse-right :pressed)
     (mouse-press :right))

    ((:mouse-right :released)
     (mouse-release :right))

    ((:mouse-left :released)
     (mouse-release :left))

    ((:mouse-left :pressed)
     (mouse-press :left)
     (dolist (panel (panels game))
       (click-event panel))))

  (bind-cursor (lambda (x y)
                 "Save cursor position"
                 (mouse-update-pos x y))))

;;; Game Act
(defmethod act ((game gk-ui-rpg-example))
  (let ((panels (panels (gamekit))))
   (dolist (panel panels)
     (panel-act panel))))


;;; Game Draw
(defmethod draw ((game gk-ui-rpg-example))
  (draw-rect (vec2 0 0)
             *screen-width*
             *screen-height*
             :fill-paint (hexcolor "#222222"))
  ;; draw help in lower left corner
  (draw-text "press 's' to toggle menu"
             (vec2 10 10)
             :fill-color (hexcolor "#FFFFFF"))
  (draw-text "press 'l' to increment xp"
             (vec2 10 30)
             :fill-color (hexcolor "#FFFFFF"))
  (draw-text "press 'd' to damage"
             (vec2 10 50)
             :fill-color (hexcolor "#FFFFFF"))
  (draw-text "press 'h' to heal"
             (vec2 10 70)
             :fill-color (hexcolor "#FFFFFF"))
  ;; draw player
  (render *player*)
  (when *show-menu-p*
    (let ((*button-draw-color* ;; (alpha-color (hexcolor "#333333") .5)
            ))
      (dolist (panel (panels game))
        (render panel)))))

;;; Exported Functions
(defun run () (start 'gk-ui-rpg-example))
