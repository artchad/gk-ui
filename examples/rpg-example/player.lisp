;;;; player.lisp
(in-package #:gk-ui-rpg-example)


(defparameter *xp-increment-amount-per-lvl* 100)

(defparameter *player-pos* (vec2 700 20)
  "Position at which the player sprite will be drawn")


(defclass player ()
  ((hp     :initarg :hp     :accessor hp     :initform nil)
   (hp-max :initarg :hp-max :accessor hp-max :initform nil)
   (mp     :initarg :mp     :accessor mp     :initform nil)
   (mp-max :initarg :mp-max :accessor mp-max :initform nil)
   (atk :initarg :atk :accessor atk :initform nil)
   (def :initarg :def :accessor def :initform nil)
   (int :initarg :int :accessor int :initform nil)
   (ini :initarg :ini :accessor ini :initform nil)
   (lvl :initarg :lvl :accessor lvl :initform 0)
   (lvls-skilled :initform 0 :accessor lvls-skilled)
   (xp :initarg :xp :accessor xp :initform 0)
   (curr-lvl-xp :initarg :curr-lvl-xp :accessor curr-lvl-xp :initform 0)
   (next-lvl-xp :initarg :next-lvl-xp :accessor next-lvl-xp :initform 100)))

(defmethod print-object ((this player) stream)
  (with-slots (hp hp-max mp mp-max atk def int ini lvl xp next-lvl-xp) this
    (print-unreadable-object (this stream :type t)
      (format stream "lvl: ~A (~A/~A)hp (~A/~A)mp" lvl hp hp-max mp mp-max))))

(defun make-player (&rest args)
  (apply #'make-instance 'player args))

(defparameter *player* nil)
;;; leveling up a stat adds 5 to that stat

(defun add-xp (amount)
  (with-accessors ((xp xp)
                   (curr-lvl-xp curr-lvl-xp)
                   (next-lvl-xp next-lvl-xp))
      *player*
    (incf xp amount)
    (incf curr-lvl-xp amount)
    (if (> curr-lvl-xp next-lvl-xp)
        (level-up))))

(defun level-up ()
  "level up `*player*'"
  (incf (lvl *player*))

  (decf (curr-lvl-xp *player*) (next-lvl-xp *player*))
  (incf (next-lvl-xp *player*) *xp-increment-amount-per-lvl*)
  )


(defun body-sprite (this)
  (cond ((> (lvl this) 20) :img-body-lvl-2)
        ((> (lvl this) 10) :img-body-lvl-1)
        (t :img-body-lvl-0)))

(defun arm-right-sprite (this)
  (cond ((> (atk this) 60) :img-arm-right-lvl-2)
        ((> (atk this) 30) :img-arm-right-lvl-1)
        (t :img-arm-right-lvl-0)))

(defun arm-left-sprite (this)
  (cond ((> (def this) 60) :img-arm-left-lvl-2)
        ((> (def this) 30) :img-arm-left-lvl-1)
        (t :img-arm-left-lvl-0)))

(defun head-sprite (this)
  (cond ((> (int this) 60) :img-head-lvl-2)
        ((> (int this) 30) :img-head-lvl-1)
        (t :img-head-lvl-0)))

(defun legs-sprite (this)
  (cond ((> (ini this) 60) :img-legs-lvl-2)
        ((> (ini this) 30) :img-legs-lvl-1)
        (t :img-legs-lvl-0)))

(defmethod render ((this player))
  (let ((body (body-sprite this))
        (arm-right (arm-right-sprite this))
        (arm-left (arm-left-sprite this))
        (head (head-sprite this))
        (legs (legs-sprite this)))
    (draw-image *player-pos* body)
    (draw-image *player-pos* arm-right)
    (draw-image *player-pos* arm-left)
    (draw-image *player-pos* head)
    (draw-image *player-pos* legs)))


(defmacro make-stat-changer (object stat &optional (amount 1))
  `(lambda () (incf (,stat ,object) ,amount)))

(defparameter player-atk-inc-1  (make-stat-changer *player* atk 1))
(defparameter player-atk-inc-5  (make-stat-changer *player* atk 5))

(defparameter player-def-inc-1  (make-stat-changer *player* def 1))
(defparameter player-def-inc-5  (make-stat-changer *player* def 5))

(defparameter player-int-inc-1  (make-stat-changer *player* int 1))
(defparameter player-int-inc-5  (make-stat-changer *player* int 5))

(defparameter player-ini-inc-1  (make-stat-changer *player* ini 1))
(defparameter player-ini-inc-5  (make-stat-changer *player* ini 5))

(defparameter player-atk-dec-1  (make-stat-changer *player* atk -1))
(defparameter player-atk-dec-5  (make-stat-changer *player* atk -5))

(defparameter player-def-dec-1  (make-stat-changer *player* def -1))
(defparameter player-def-dec-5  (make-stat-changer *player* def -5))

(defparameter player-int-dec-1  (make-stat-changer *player* int -1))
(defparameter player-int-dec-5  (make-stat-changer *player* int -5))

(defparameter player-ini-dec-1  (make-stat-changer *player* ini -1))
(defparameter player-ini-dec-5  (make-stat-changer *player* ini -5))

(defmethod damage-for (amount (p player))
  (setf (hp p) (alexandria:clamp (- (hp p) amount) 0 (hp-max p))))

(defmethod heal-for (amount (p player))
  (setf (hp p) (alexandria:clamp (+ (hp p) amount) 0 (hp-max p))))
