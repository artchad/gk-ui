;;;; basic-example.lisp

(in-package #:gk-ui-basic-example)


;;; Utils
(defun hexcolor (hexcode &optional (a 1))
  (let* ((color-string (subseq hexcode 1))
         (r (float (/ (parse-integer (subseq color-string 0 2) :radix 16) 255)))
         (g (float (/ (parse-integer (subseq color-string 2 4) :radix 16) 255)))
         (b (float (/ (parse-integer (subseq color-string 4 6) :radix 16) 255))))
    (vec4 r g b a)))


;;; Global Variables
(defparameter *screen-width* 512)
(defparameter *screen-height* 768)
(defparameter *panel-width* 200)
(setf gk-ui:*ui-font-color* (hexcolor "#8791a3"))

;;; Resources
(defun asset-path (pathname)
  (asdf:system-relative-pathname :gk-ui/examples (merge-pathnames pathname "examples/basic-example/assets/")))

(register-resource-package :gk-ui-basic-example (asset-path ""))
(define-image :img-ok-sign (asset-path "ok-sign.png"))
(define-image :img-not-ok-sign (asset-path "not-ok-sign.png"))

;;; Define Game
(defgame gk-ui-basic-example ()
  ((panel :accessor panel
          :initform (make-instance 'panel
                                   :origin (vec2 40 0)
                                   :width *panel-width*
                                   :height *screen-height*
                                   :padding 20)))
  (:viewport-width *screen-width*)
  (:viewport-height *screen-height*))


;;; Update Methods
(defmethod update-label-alive ((label label)))
(defmethod update-label-dead ((label label)))
(defmethod update-label-sick ((label label)))
(defmethod update-label-killers ((label label)))
(defmethod update-status-bar ((bar status-bar)))


;;; Post Init
(defmethod post-initialize ((game gk-ui-basic-example))
  ;; UI elements
  (let* ((title (make-instance 'label :text "Such Is Life"
                                      :no-padding t))
         (subtitle (make-instance 'label :text "Lisp Game Jam"
                                         :no-padding t))

         (alive (make-instance 'label :text "Alive: ?"
                                      ;; :text-align :left
                                      :status-image :img-ok-sign
                                      :update #'update-label-alive))
         (dead (make-instance 'label :text "Dead: ?"
                                     :status-image :img-ok-sign
                                     :no-padding t
                                     ;; :text-align :left
                                     :update #'update-label-dead))
         (sick (make-instance 'label :text "Sick: ?"
                                     :status-image :img-ok-sign
                                     :no-padding t
                                     ;; :text-align :left
                                     :update #'update-label-sick))
         (killers (make-instance 'label :text "Killers: ?"
                                        :status-image :img-ok-sign
                                        :no-padding t
                                        ;; :text-align :left
                                        :update #'update-label-killers))

         (status-bar (make-instance 'status-bar
                                    :no-padding t
                                    :total-amount 10
                                    :update #'update-status-bar))

         (restart-button (make-button :width 160
                                      :height 20
                                      :text "Restart"
                                      :action (lambda () (princ "Restarted Game."))))
         (size-adjuster
           (make-instance 'adjuster
                          :allowed-values '(10 15 20 25 30 40 50)
                          :current-value 10
                          :text "Size"
                          :action (lambda (value) (princ "adjusted Size."))))

         (population-adjuster
           (make-instance 'adjuster
                          :allowed-values '(1 2 3 4 5 7 10 15 20 30 40)
                          :current-value 15
                          :text "People %"
                          :action (lambda (value) (princ "adjusted People %"))))

         (sick-adjuster
           (make-instance 'adjuster
                          :allowed-values '(0 1 5 10 15 20 30 40 50 60 70 80 90 100)
                          :current-value 90
                          :text "Sick %"
                          :action (lambda (value) (princ "adjusted Sick %"))))

         (police-adjuster
           (make-instance 'adjuster
                          :allowed-values '(0 1 2 3 4 5 6 7 8 9 10)
                          :current-value 9
                          :text "Police"
                          :action (lambda (value) (princ "adjusted Police %"))))

         (medics-adjuster
           (make-instance 'adjuster
                          :allowed-values '(0 1 2 3 4 5 6 7 8 9 10)
                          :current-value 8
                          :text "Medics"
                          :action (lambda (value) (princ "adjusted Medics %"))))
         ;; This line only works, because we're inside a `let*'.
         (*button-base-color* (hexcolor "#2C633B"))

         (killers-adjuster
           (make-instance 'adjuster
                          :allowed-values '(0 1 2 3 4 5 6 7 8 9 10)
                          :current-value 4
                          :text "Killers"
                          :action (lambda (value) (princ "adjusted Killers"))))

         (cough-frequency-adjuster
           (make-instance 'adjuster
                          :allowed-values '(0.5 1 2 3 4 5 6 7 8 9 10)
                          :current-value 10
                          :text "Rate"
                          :action (lambda (value) (princ "adjusted Rate"))))

         (cough-damage-adjuster
           (make-instance 'adjuster
                          :allowed-values '(0.1 0.5 1 2 5 10)
                          :current-value 1
                          :text "Damage"
                          :action (lambda (value) (princ "adjusted Damage")))))

    (add-elements (panel game)
      title
      subtitle
      (make-instance 'separator)
      (make-instance 'label :text "Statistics")
      alive
      dead
      sick
      killers
      status-bar
      (make-instance 'separator)
      (make-instance 'label :text "Settings")
      size-adjuster
      population-adjuster
      sick-adjuster
      medics-adjuster
      police-adjuster
      killers-adjuster
      (make-instance 'separator)
      (make-instance 'label :text "Virus...")
      cough-frequency-adjuster
      cough-damage-adjuster
      (make-instance 'separator)
      restart-button))


  (bind-button :escape :pressed #'gamekit:stop)
  (bind-button :q :pressed #'gamekit:stop)


  ;; Mouse related things
  (bind-cursor (lambda (x y)
                 "Save cursor position"
                 (mouse-update-pos x y)))

  (bind-button :mouse-right :pressed
               (lambda () (mouse-press :right)))
  (bind-button :mouse-right :released
               (lambda () (mouse-release :right)))

  (bind-button :mouse-left :released
               (lambda () (mouse-release :left)))
  (bind-button :mouse-left :pressed
               (lambda () (mouse-press :left)
                 (click-event (panel game)))))

(defmethod act ((game gk-ui-basic-example))
  (panel-act (panel game)))

(defmethod draw ((game gk-ui-basic-example))
  (draw-rect (vec2 0 0)
             *screen-width*
             *screen-height*
             :fill-paint (hexcolor "#223365"))
  (let ((*button-draw-color* ; (hexcolor "#007765")
          ))
    (render (panel game))))

(defun run () (start 'gk-ui-basic-example))
