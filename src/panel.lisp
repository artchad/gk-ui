(cl:in-package :gk-ui)

(defparameter *ui-font* nil)
(defparameter *ui-font-color* nil)

;;; Utility Functions for drawing the elements the panel consists of
(defmethod draw-button-with-text (origin width height text &key fill-color roundness)
  (with-pushed-canvas ()
    (draw-rect origin width height :fill-paint fill-color :rounding roundness)
    (draw-text-centered origin width height text)))

(defmethod draw-round-button-with-text (origin radius text fill)
  (with-pushed-canvas ()
    (draw-circle (add origin (vec2 radius radius)) radius :fill-paint fill)
    (draw-text-centered origin (* 2 radius) (* 2 radius) text)))

(defmethod draw-text-aligned (origin width height text aligned)
  (ecase aligned
    (:center (draw-text-centered origin width height text))
    (:left   (draw-text-left     origin width height text))
    (:right  (draw-text-right    origin width height text))))

(defmethod draw-text-centered (origin width height text)
  (let* ((text-offset-x (- (/ width 2) (/ (text-width text) 2)))
         (text-offset-y (- (/ height 2) (/ (text-height text) 3)))
         (text-origin-x (+ (x origin) text-offset-x))
         (text-origin-y (+ (y origin) text-offset-y)))
    (draw-text text (vec2 text-origin-x text-origin-y)
               :font *ui-font*
               :fill-color *ui-font-color*)))

(defmethod draw-text-left (origin width height text)
  (let* ((text-offset-x 0)
         (text-offset-y (- (/ height 2) (/ (text-height text) 2)))
         (text-origin-x (+ (x origin) text-offset-x))
         (text-origin-y (+ (y origin) text-offset-y)))
    (draw-text text (vec2 text-origin-x text-origin-y)
               :font *ui-font*
               :fill-color *ui-font-color*)))

(defmethod draw-text-right (origin width height text)
  (let* ((text-offset-x (- width (text-width text)))
         (text-offset-y (- (/ height 2) (/ (text-height text) 2)))
         (text-origin-x (+ (x origin) text-offset-x))
         (text-origin-y (+ (y origin) text-offset-y)))
    (draw-text text (vec2 text-origin-x text-origin-y)
               :font *ui-font*
               :fill-color *ui-font-color*)))

(defun text-width (text)
  (nth-value 1 (calc-text-bounds text)))

(defun text-height (text)
  (nth-value 2 (calc-text-bounds text)))


(defclass panel (element)
  ((align :initform :center :initarg :align :accessor align
          :documentation "This alignment will be the default alignment~
                          for any elements added to this panel.")
   (width :initarg :width
          :reader width
          :initform (error "A panel needs a width."))
   (height :initarg :height
           :reader height
           :initform (error "A panel needs a height."))
   (origin :initarg :origin :accessor origin)

   (padding :initform 0 :initarg :padding :reader padding
            :documentation "The padding at the top of the panel.")
   (height-occupied :initform 0 :accessor height-occupied)
   (elements :initarg :elements :initform '() :accessor elements)
   (bg-color :initarg :bg-color :accessor bg-color :initform nil)
   (index :initform 0 :accessor index)))

(defun make-panel (&rest args) (apply #'make-instance 'panel args))

(defmethod initialize-instance :after ((panel panel) &key)
  (setf (height-occupied panel) (padding panel)))

(defmethod print-object ((this panel) stream)
  (flet ((elm->short-str (elm)
           (ecase (type-of elm)
             ('button "button")
             (adjuster "adj")
             (label "label")
             (dynamic-label "dlabel")
             (static-label "slabel")
             (panel "panel")
             (separator "sep")
             (status-bar "sbar"))))
   (print-unreadable-object (this stream)
     (format stream "~{~A~^, ~}" (mapcar #'elm->short-str (elements this))))))


(defmethod add-element ((panel panel) (element element))
  "Adds an `element' to `panel' below the previously added `element'.
So the panel is effectively filled from top to bottom."
  (with-accessors ((p-width width)
                   (p-height height)
                   (index index)
                   (padding padding)
                   (p-origin origin)
                   (p-align align)
                   (height-occupied height-occupied))
      panel
    (with-accessors ((e-width width)
                     (e-height height)
                     (e-origin origin)
                     (e-offset offset)
                     (e-no-padding no-padding)
                     (e-align align))
        element
     (let* ((top-left-corner-height (+ (y p-origin) p-height))
            (element-height (* (row-span element) *element-base-height*))
            (element-y (- top-left-corner-height
                          height-occupied
                          element-height)))
       (when (and (> index 0) (not e-no-padding))
         (incf element-height *element-padding*)
         (decf element-y *element-padding*))
       (incf index)
       (incf height-occupied element-height)

       (unless e-align (setf e-align p-align))
       (unless e-width
         (setf e-width p-width))
       (unless e-height
         (setf e-height element-height))
       (unless e-offset (setf e-offset (vec2 0 0)))

       (let ((e-origin-x (+ (x e-offset) (x p-origin)))
             (e-origin-y (y e-offset)))
        (flet ((align->x-offset (a)
                 (ecase a
                   (:center (- (/ p-width 2) (/ e-width 2)))
                   (:left   0)
                   (:right  (- p-width e-width)))))
          (let ((align-offset (vec2 (+ (align->x-offset e-align) e-origin-x)
                                    (+ element-y e-origin-y))))
            (setf e-origin align-offset))))

       (push element (elements panel))))))

(defmacro add-elements (panel &body elements)
  (let (result)
    (dolist (element elements `(progn ,@(nreverse result)
                                      (setf (elements ,panel)
                                            ;; we still need to reverse the list
                                            ;; because `add-element' uses `push'.
                                            (reverse (elements ,panel)))))
      (push `(add-element ,panel ,element) result))))

(defmethod panel-act ((this panel))
   "this function is special, because a panel does not
 inherit from `element' but rather acts as a container
 for multiple UI objects that inherit from `element'."
    (dolist (e (elements this))
    (element-act e)))

(defmethod render ((this panel))
  (with-slots (bg-color origin offset width height elements) this
    (when bg-color
      (draw-rect (add origin (or offset 0)) width height :fill-paint bg-color))
    (dolist (e elements) (render e))))

(defmethod click-event ((panel panel))
  "Calls `click-event' on every element of `panel'.
The click event is obligated to decide weather or not anything actually happens."
  (dolist (e (elements panel))
    (click-event e)))
