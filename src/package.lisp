;;; All UI elements need a `panel' as a wrapping container.

(defpackage #:gk-ui
  (:nicknames #:ui)
  (:use #:cl #:trivial-gamekit)
  (:export
   #:+black+
   #:+white+
   ;; some colors
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   "MOUSE"
   ;; dynamic variables
   #:*mouse*
   ;; other functions and methods
   #:mouse-left-button-pressed-p
   #:mouse-right-button-pressed-p
   #:mouse-middle-button-pressed-p
   #:mouse-no-button-pressed-p
   #:mouse-pos
   #:mouse-update-pos
   #:mouse-press
   #:mouse-release
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   "ELEMENT"
   ;; dynamic variables
   #:*element-base-height*
   #:*element-base-width*
   #:*element-padding*
   #:*ui-font*
   #:*ui-font-color*
   ;; accessors
   #:text
   #:action
   #:origin
   #:row-span
   #:no-padding
   #:height
   #:width
   ;; other functions and methods
   #:element-act
   #:click-event
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   "BUTTON"
   ;; dynamic variables
   #:*button-base-width*
   #:*button-base-height*
   #:*button-base-color*
   #:*button-draw-color*
   ;; accessors
   #:state
   #:base-color
   #:roundness
   ;; other functions and methods
   #:make-button
   #:button-current-color
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   "ADJUSTER"
   ;; dynamic variables
   #:*decb-offset-x* ; decrease-button-offset-x
   ;; accessors
   #:allowed-values
   #:current-value
   #:row-span
   #:increase-button
   #:decrease-button
   ;; other functions and methods
   #:next-value
   #:previous-value
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   "LABEL"
   ;; dynamic variables
   ;; accessors
   #:status-image
   #:text-align
   #:update
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   "LABEL"
   ;; other functions and methods
   #:make-static-label
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   "DYNAMIC-LABEL"
   ;;accessors
   #:format-str
   #:format-variables
   ;; macros
   #:make-dynamic-label
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   "STATUS-BAR"
   ;; dynamic variables
   #:*status-bar-base-filled-amount-0-color*
   #:*status-bar-base-filled-amount-1-color*
   #:*status-bar-base-max-amount-color*
   #:*status-bar-base-stroke-color*
   ;; accessors
   #:filled-amount-0
   #:filled-amount-0-color
   #:filled-amount-1
   #:filled-amount-0-color
   #:max-amount
   #:max-amount-color
   #:stroke-color
   ;; other functions and methods
   #:make-status-bar
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   "SEPARATOR"
   ;; other functions and methods
   #:make-separator
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   "PANEL"
   ;; accessors
   #:padding
   #:height-occupied
   #:elements
   #:bg-color
   #:index
   ;; other functions and methods
   #:make-panel
   #:add-element
   #:add-elements
   #:panel-act
   #:render))

(defpackage #:gk-ui-basic-example
  (:use #:cl #:trivial-gamekit #:gk-ui)
  (:export #:run))

(defpackage #:gk-ui-rpg-example
  (:use #:cl #:trivial-gamekit #:gk-ui)
  (:export #:run))
