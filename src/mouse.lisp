(in-package #:gk-ui)

(defclass mouse ()
  ((pos :initform (vec2 0 0)
        :initarg :pos
        :accessor pos-of)
   (state :initform (vector nil nil nil)
          :initarg :state
          :accessor state-of
          :documentation "
1st elm: left pressed
2nd elm: right pressed
3rd elm: middle pressed"))
  (:documentation "Represents the players mouse."))

(defparameter *mouse* (make-instance 'mouse))


(defun mouse-left-button-pressed-p ()
  "API"
  (not (null (elt (state-of *mouse*) 0))))

(defun mouse-right-button-pressed-p ()
  "API"
  (not (null (elt (state-of *mouse*) 1))))

(defun mouse-middle-button-pressed-p ()
  "API"
  (not (null (elt (state-of *mouse*) 2))))

(defun mouse-no-button-pressed-p ()
  "API"
  (and (not (mouse-left-button-pressed-p))
       (not (mouse-right-button-pressed-p))
       (not (mouse-middle-button-pressed-p))))

(defun mouse-pos ()
  "API"
  (pos-of *mouse*))

(defun mouse-update-pos (x y)
  "API"
  (let ((new-pos (vec2 x y)))
    (setf (pos-of *mouse*) new-pos)))


(defun mouse-press (mouse-button)
  "API
:left :right :middle"
  (ecase mouse-button
    (:left   (setf (elt (state-of *mouse*) 0) t))
    (:right  (setf (elt (state-of *mouse*) 1) t))
    (:middle (setf (elt (state-of *mouse*) 2) t))))

(defun mouse-release (mouse-button)
  "API
:left :right :middle"
  (ecase mouse-button
    (:left   (setf (elt (state-of *mouse*) 0) nil))
    (:right  (setf (elt (state-of *mouse*) 1) nil))
    (:middle (setf (elt (state-of *mouse*) 2) nil))))
