(cl:in-package :gk-ui)

(defparameter *decb-offset-x* 80
  "The offset of an adjusters `decrease-button' in relation
to the right edge of the `increase-button'.")

(defclass adjuster (element)
  ((allowed-values :initarg :allowed-values
                   :initform (error "Adjuster element needs allowed values")
                   :accessor allowed-values)
   (current-value :initarg :current-value
                  :initform (error "Adjuster element needs current value")
                  :accessor current-value
                  :documentation "make sure it's one of `allowd-values'")
   (text :initarg :text :initform "Some text" :accessor text)
   (row-span :initform 1 :accessor row-span)
   (action :initform nil :initarg action :accessor action)
   (increase-button :initarg :increase-button
                    :accessor increase-button
                    :initform (make-button))
   (decrease-button :initarg :decrease-button
                    :accessor decrease-button
                    :initform (make-button))))

(defmethod print-object ((this adjuster) stream)
  (print-unreadable-object (this stream :type t)
    (format stream "~S" (text this))))

(defmethod next-value ((this adjuster))
  "Set the `current-value' to the next value in the `allowed-values' list.
After that, if `this' adjuster has an `action', it will call that action."
  (let* ((current-value (current-value this))
         (index (position current-value (allowed-values this))))
    (when (and
           index
           (< index (1- (length (allowed-values this)))))
      (setf (current-value this) (nth (1+ index) (allowed-values this)))
      (when (action this)
        (funcall (action this) (current-value this))))))

(defmethod previous-value ((this adjuster))
  "Set the `current-value' to the previous value in the `allowed-values' list.
After that, if `this' adjuster has an `action', it will call that action."
  (let* ((current-value (current-value this))
         (index (position current-value (allowed-values this))))
    (when (and
           index
           (> index 0))
      (setf (current-value this) (nth (1- index) (allowed-values this)))
      (when (action this)
        (funcall (action this) (current-value this))))))


(defmethod element-act ((this adjuster))
  "Makes sure the state of the `increase-button' and
the `decrease-button' are correct in relation to `*mouse*'."
  (let* ((cursor-pos (mouse-pos))
         (x (x cursor-pos))
         (y (y cursor-pos)))
    (cond ((not (and
                 (> x (+ (x (origin this))
                         *decb-offset-x*))
                 (< x (+ (x (origin this))
                         *decb-offset-x*
                         *element-base-height*))
                 (> y (y (origin this)))
                 (< y (+ (y (origin this)) *element-base-height*))))
           (setf (state (decrease-button this)) :up))
          ((mouse-left-button-pressed-p)
           (setf (state (decrease-button this)) :down))
          (t
           (setf (state (decrease-button this)) :hover)))
    (cond ((not (and
                 (> x (+ (x (origin this))
                         (- (width this) *element-base-height*)))
                 (< x (+ (x (origin this))
                         (width this)))
                 (> y (y (origin this)))
                 (< y (+ (y (origin this)) *element-base-height*))))
           (setf (state (increase-button this)) :up))
          ((mouse-left-button-pressed-p)
           (setf (state (increase-button this)) :down))
          (t
           (setf (state (increase-button this)) :hover)))))

(defmethod render ((a adjuster))
  (with-accessors ((inc-b increase-button)
                   (dec-b decrease-button))
      a
   (with-pushed-canvas ()
     (translate-canvas-vec (origin a))

     (draw-text-left (vec2 0 0)
                     (width a)
                     *element-base-height*
                     (text a))
     (draw-round-button-with-text (vec2 (- (width a) *element-base-height*) 0)
                                  (/ *element-base-height* 2)
                                  "+"
                                  (button-current-color inc-b))
     (let* ((text (write-to-string (current-value a)))
            (text-offset-x (+ *decb-offset-x* *element-base-height*)))
       (draw-text-centered (vec2 text-offset-x 0)
                           (- (- (width a) *element-base-height*)
                              text-offset-x)
                           *element-base-height*
                           text)
       (draw-round-button-with-text (vec2 *decb-offset-x*
                                          0)
                                    (/ *element-base-height* 2)
                                    "-"
                                    (button-current-color dec-b))))))

(defmethod click-event ((this adjuster))
  (let* ((cursor-pos (mouse-pos))
         (x (x cursor-pos))
         (y (y cursor-pos)))
    (when
        (and
         (> x (+ (x (origin this))
                 *decb-offset-x*))
         (< x (+ (x (origin this))
                 *decb-offset-x*
                 *element-base-height*))
         (> y (y (origin this)))
         (< y (+ (y (origin this)) *element-base-height*)))
      (previous-value this))
    (when
        (and
         (> x (+ (x (origin this)) (- (width this) *element-base-height*)))
         (< x (+ (x (origin this)) (width this)))
         (> y (y (origin this)))
         (< y (+ (y (origin this)) *element-base-height*)))
      (next-value this))))
