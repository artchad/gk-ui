;;;; element.lips
;;;; This is a generic UI element superclass.
(in-package #:gk-ui)

;;; utility stuff
(defvar +black+ (vec4 0 0 0 1))
(defvar +white+ (vec4 1 1 1 1))

(defparameter *element-base-height* 25)
(defparameter *element-base-width* 25)
(defparameter *element-padding* 10)

(defun translate-canvas-vec (vec)
  (translate-canvas (x vec) (y vec)))

;;; element
(defclass element ()
  ((align :initform nil :initarg :align :accessor align
          :documentation "either :left :right :center")
   (height :initform nil :accessor height :initarg :height)
   (width  :initform nil :accessor width :initarg :width)
   (text :initform "Some text" :initarg :text :accessor text)
   (font :initform nil :initarg :font :accessor font)
   (action :initform nil :initarg :action :accessor action)
   (origin :initform nil :accessor origin)
   (offset :initarg :offset :accessor offset :initform nil)
   (row-span :initform 1 :initarg :row-span :accessor row-span)
   (no-padding :initform nil :initarg :no-padding :accessor no-padding)))

(defmethod element-act ((element element))
  "This method is the base method for static UI elements.")

(defgeneric click-event (element)
  (:method ((element element))
    "This method will be the primary method for static UI elements that
don't need react to click events and therefore
don't define their own `click-event' method.")
  (:documentation "Specifies what to do do based on the (cursor-pos)."))
