(in-package :gk-ui)


(defparameter *status-bar-base-filled-amount-0-color* (vec4 0.5 0.86 0.02 1))
(defparameter *status-bar-base-filled-amount-1-color* (vec4 0.77 0.03 0.03 1))
(defparameter *status-bar-base-max-amount-color* (vec4 0 0 0 0.3))
(defparameter *status-bar-base-stroke-color* (vec4 0 0 0 1))

(defclass status-bar (element)
  ((filled-amount-0 :initarg :filled-amount-0
                    :accessor filled-amount-0)
   (filled-amount-0-color :initarg :filled-amount-0-color
                          :accessor filled-amount-0-color
                          :initform nil)
   (filled-amount-1 :initarg :filled-amount-1
                    :accessor filled-amount-1)
   (filled-amount-1-color :initarg :filled-amount-1-color
                          :accessor filled-amount-1-color
                          :initform nil)
   (max-amount :initarg :max-amount
               :accessor max-amount)
   (max-amount-color :initarg :max-amount-color
                     :accessor max-amount-color
                     :initform nil)
   (stroke-color :initarg :stroke-color
                 :accessor stroke-color
                 :initform nil)

   (padding-vertical :accessor padding-vertical
                     :initarg :padding-vertical
                     :initform 7 )
   (update :initarg :update
           :accessor update
           :initform nil
           :documentation "a function object that takes exactly one argument"))
  (:default-initargs
   ;; make sure `max-amount' is not 0, otherwise you will divide by zero which causes an error
   :max-amount 1

   :filled-amount-0-color *status-bar-base-filled-amount-0-color*
   :filled-amount-1-color *status-bar-base-filled-amount-1-color*
   :max-amount-color *status-bar-base-max-amount-color*
   :stroke-color *status-bar-base-stroke-color*

   :filled-amount-0 0
   :filled-amount-1 0))

(defun make-status-bar (&rest args) (apply #'make-instance 'status-bar args))

(defmethod fill-bar ((this status-bar) amount-0 &optional (amount-1 0))
  (alexandria:clamp amount-0 0 (max-amount this))
  (alexandria:clamp amount-1 0 (max-amount this)))

(defmethod initialize-instance :after ((this status-bar)
                                       &key
                                         (max-amount
                                          nil
                                          max-amount-suplied-p)
                                       &allow-other-keys)
  (if max-amount-suplied-p
      (let ((filled-amount (+ (filled-amount-0 this)
                              (filled-amount-1 this))))
        (when (< max-amount filled-amount)
          (setf (max-amount this) filled-amount)))))


(defmethod element-act ((bar status-bar))
  (when (update bar)
    (funcall (update bar) bar)))

(defmethod render ((bar status-bar))
  (with-pushed-canvas ()
    (translate-canvas-vec (origin bar))
    (let* ((fa0 (filled-amount-0 bar))
           (fa1 (filled-amount-1 bar))
           (total  (max-amount bar))
           (padding-vertical (padding-vertical bar))
           (width (width bar))
           (height (- *element-base-height* (* 2 padding-vertical)))
           (offset-y (- (/ *element-base-height* 2) (/ height 2)))
           (fa0-width (* width (/ fa0 total)))
           (fa1-width (* width (/ fa1 total)))
           (rest-width (* width (/ (- total fa0 fa1) total))))
      ;; draw bounding box
      (draw-rect (vec2 0 offset-y) width height
                 :stroke-paint *status-bar-base-stroke-color*
                 :fill-paint nil
                 :thickness 1)

      (draw-rect (vec2 0 offset-y) fa0-width height
                 :stroke-paint nil
                 :fill-paint (or (filled-amount-0-color bar) *status-bar-base-filled-amount-0-color*))
      (draw-rect (vec2 fa0-width offset-y) fa1-width height
                 :stroke-paint nil
                 :fill-paint (or (filled-amount-1-color bar) *status-bar-base-filled-amount-1-color*))
      (draw-rect (vec2 (+ fa0-width fa1-width) offset-y) rest-width height
                 :stroke-paint nil
                 :fill-paint (or (max-amount-color bar) *status-bar-base-max-amount-color*)
                 :stroke-paint (or (stroke-color bar) *status-bar-base-stroke-color*)))))
