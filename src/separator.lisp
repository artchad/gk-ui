(in-package :gk-ui)

(defclass separator (element)
  ((thickness :initform 1 :initarg :thickness :accessor thickness)
   (row-span :initform 0.2 :initarg :row-span :accessor row-span)))

(defun make-separator (&rest args)
  (apply #'make-instance 'separator args))

(defmethod print-object ((this separator) stream)
  (print-unreadable-object (this stream :type t)
    (format stream "row-span: ~S" (row-span this))))

(defmethod render ((sep separator))
  (with-pushed-canvas ()
    (translate-canvas-vec (origin sep))
    (let* ((sep-height (* (row-span sep) *element-base-height*))
           (sep-y (/ sep-height 2)))
      (draw-line (vec2 0 sep-y)
                 (vec2 (width sep) sep-y)
                 +black+
                 :thickness (thickness sep)))))
