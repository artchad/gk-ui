(cl:in-package :gk-ui)

(defparameter *button-base-width* *element-base-width*)
(defparameter *button-base-height* *element-base-height*)
(defparameter *button-base-color* (vec4 .5 .5 .5 1))

(defparameter *button-draw-color* nil
  "The color used by the `render' method.
Supersedes th buttons `base-color' slot if `base-color' non-nil.
 TYPE: gamekit:vec4")

(defclass button (element)
  ((state :accessor state
          :initarg :state
          :initform :up
          :documentation "Either :up :down or :hover.")
   (base-color :accessor base-color
               :initarg :base-color
               :initform (error "a button needs a base-color"))
   (roundness :initarg :roundness :accessor roundness))
  (:default-initargs
   :width *button-base-width*
   :height *button-base-height*
   :base-color *button-base-color*
   :text ""
   :state :up
   :roundness 0
   :row-span 1))


(defun make-button (&rest args)
  "Creates a new `button' instance.
If `base-color' is not specified it will use *button-base-color*.
If `width' is not specified it will use *button-base-width*.
If `height' is not specified it will use *button-base-height*."
  (apply #'make-instance 'button args))


(defmethod print-object ((this button) stream)
  (with-accessors ((s state) (w width) (h height) (bc base-color)) this
   (print-unreadable-object (this stream :type t)
     (format stream "~S w:~A h:~A (RGBA: ~A ~A ~A ~A)"
             s w h (x bc) (y bc) (z bc) (w bc)))))


(defmethod button-current-color ((button button))
  "Returns color of the button based on `base-color',
taking into account the buttons `state'."
  (if (null *button-draw-color*)
      (with-accessors ((bc base-color)) button
        (ecase (state button)
          (:up    (with-bright 1 bc))
          (:down  (with-bright .8 bc))
          (:hover (with-bright .9  bc))))
      (ecase (state button)
        (:up    (with-bright 1 *button-draw-color*))
        (:down  (with-bright .8 *button-draw-color*))
        (:hover (with-bright .9  *button-draw-color*)))))

(defmethod element-act ((this button))
  "Defines the behaviour of a button each frame."
  (let ((x (x (mouse-pos)))
        (y (y (mouse-pos))))
    (cond ((not
            (and
             (> x (x (origin this)))
             (< x (+ (x (origin this)) (width this)))
             (> y (y (origin this)))
             (< y (+ (y (origin this)) (height this)))))
           (setf (state this) :up))
          ((mouse-left-button-pressed-p)
           (setf (state this) :down))
          (t
           (setf (state this) :hover)))))

(defmethod render ((button button))
  "Draw the `button' appropriately."
  (with-pushed-canvas ()
    (translate-canvas-vec (origin button))
    (draw-button-with-text (vec2 0 0)
                           (width button)
                           (height button)
                           (text button)
                           :fill-color (button-current-color button)
                           :roundness (roundness button))))

(defmethod click-event ((this button))
  "Checks weather the `*mouse*' cursor is on `this' button and
calls the buttons `action' if so."
  (when (action this) ; only do the calculation if the button has an action
   (let* ((cursor-pos (mouse-pos))
          (x (x cursor-pos))
          (y (y cursor-pos)))
     (when
         (and
          (> x (x (origin this)))
          (< x (+ (x (origin this)) (width this)))
          (> y (y (origin this)))
          (< y (+ (y (origin this)) *element-base-height*)))
       (funcall (action this))))))
