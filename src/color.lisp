(in-package #:gk-ui)

(defun alpha-color (color alpha)
  (vec4 (x color) (y color) (z color) alpha))

(defun random-color ()
  (vec4 (/ (random 100) 100)
            (/ (random 100) 100)
            (/ (random 100) 100)
            1))

(defun hexcolor (hexcode &optional (a 1))
  (let* ((color-string (subseq hexcode 1))
         (r (float (/ (parse-integer (subseq color-string 0 2) :radix 16) 255)))
         (g (float (/ (parse-integer (subseq color-string 2 4) :radix 16) 255)))
         (b (float (/ (parse-integer (subseq color-string 4 6) :radix 16) 255))))
    (vec4 r g b a)))


(defmacro with-bright (brightness-factor color)
  (if (= 1 brightness-factor)
      color
      `(vec4 (* (x ,color) ,brightness-factor)
             (* (y ,color) ,brightness-factor)
             (* (z ,color) ,brightness-factor)
             (w ,color))))
