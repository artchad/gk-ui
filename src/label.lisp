(cl:in-package :gk-ui)

(defclass label (element)
  ((status-image :initarg :status-image :initform nil :accessor status-image)
   (update :initform nil :initarg :update :accessor update))
  (:default-initargs
   :text "label"))

(defclass static-label (label) ())

(defun make-static-label (&rest args)
  (apply #'make-instance 'static-label args))

(defclass dynamic-label (label)
  ((format-str :initarg :format-str
                :accessor format-str
               :initform nil)
   (format-variables :initarg :format-variables
                     :accessor format-variables))
  (:default-initargs
   :text nil))

(defmacro make-dynamic-label ((format-str &rest str-args) &rest args)
  (if args
      `(apply #'make-instance 'dynamic-label :format-str ,format-str
                                             :format-variables ',str-args
                                             ,(cons 'list args))
      `(apply #'make-instance 'dynamic-label :format-str ,format-str
                                             :format-variables '(,@str-args))))

;; (setf test-dyn-label (make-dynamic-label ("hello world ~A ~A" 'x 'y)
;;                                          :width 20
;;                                          :height 40))


(defmethod initialize-instance :after ((this label) &key))

(defmethod print-object ((this label) stream)
  (print-unreadable-object (this stream :type t)
    (format stream "~S" (text this))))

(defmethod print-object ((this dynamic-label) stream)
  (print-unreadable-object (this stream :type t)

    (apply #'format stream (format-str this) (mapcar #'form-to-string (form-to-string elm)))))

(defmethod print-object ((this static-label) stream)
  (print-unreadable-object (this stream :type t)
    (format stream "~A" (text this))))

(defmethod element-act ((label label))
  (when (update label)
    (funcall (update label) label)))

(defmethod render ((label label))
  (with-pushed-canvas ()
    (translate-canvas-vec (origin label))
    (draw-text-aligned (vec2 0 0)
                       (width label)
                       (height label)
                       (text label)
                       (align label))
    (when (status-image label)
      (let* ((text-height (text-height (text label)))
             (scale (/ text-height (image-height (status-image label)))))
        (scale-canvas scale scale)
        (draw-image (vec2 (* (/ 1 scale)
                             (- (width label) text-height))
                          (/ (text-height (text label)) 2))
                    (status-image label))))))

(defmethod render ((label dynamic-label))
  (with-pushed-canvas ()
    (translate-canvas-vec (origin label))
    (let ((*ui-font* (or (font label) *ui-font*)))
     (draw-text-aligned (vec2 0 0)
                        (width label)
                        (height label)
                        (apply #'format nil (format-str label) (mapcar #'eval (format-variables label)))
                        (align label)))
    (when (status-image label)
      (let* ((text-height (text-height (text label)))
             (scale (/ text-height (image-height (status-image label)))))
        (scale-canvas scale scale)
        (draw-image (vec2 (* (/ 1 scale)
                             (- (width label) text-height))
                          (/ (text-height (text label)) 2))
                    (status-image label))))))
