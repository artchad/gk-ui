(asdf:defsystem #:gk-ui
  :description "Trivial-Gamekit User Interface"
  :author "decent-username <decent-username@decent-userna.me>"
  :license "GPLv3"
  :version "0.0.1"
  :serial t
  :depends-on (#:trivial-gamekit)
  :pathname "src"
  :components ((:file "package")
               (:file "mouse")
               (:file "color")

               (:file "element")
               (:file "button")
               (:file "adjuster")
               (:file "label")
               (:file "panel")
               (:file "status-bar")
               (:file "separator")))


(asdf:defsystem #:gk-ui/examples
  :description "A simple example to demonstrate how to use gk-ui."
  :author "decent-username <decent-username@decent-userna.me>"
  :license "GPLv3"
  :depends-on (#:trivial-gamekit #:gk-ui)
  :pathname "examples"
  :serial t
  :components ((:module "basic-example/"
                        :components
                        ((:file "basic-example")))
               (:module "rpg-example/"
                        :serial t
                        :components
                        ((:file "utils")
                         (:file "resources")
                         (:file "player")
                         (:file "menu")
                         (:file "rpg-example")))))
